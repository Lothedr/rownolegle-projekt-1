﻿#include "WonderLand.h"
#include "Guild.h"
#include "ElderScrolls.h"

void WonderLand::Start(Configuration configuration)
{
	ElderScrolls::Write(L"Wstaje dzień w krainie czarów.");
	_warehouse = new Warehouse(configuration.Factories);
	for (auto && guild : configuration.Guilds)
	{
		_guilds.push_back(new Guild(_warehouse, guild));
	}
	for (auto guild : _guilds)
	{
		guild->Start();
	}
	for (auto && warlock : configuration.Warlocks)
	{
		_warlocks.push_back(new Warlock(_warehouse->Factories(), warlock));
	}
	for (auto && wizard : configuration.Wizards)
	{
		_wizards.push_back(new Wizard(_warehouse->Factories(), wizard));
	}
}

void WonderLand::Stop()
{
	for (auto warlock : _warlocks)
	{
		delete warlock;
	}
	_warlocks.clear();
	for (auto wizard : _wizards)
	{
		delete wizard;
	}
	_wizards.clear();
	for (auto guild : _guilds)
	{
		guild->Stop();
	}
	delete _warehouse;
	for (auto guild : _guilds)
	{
		guild->WaitForAlchemists();
		delete guild;
	}
	_guilds.clear();
	ElderScrolls::Write(L"Zapada noc w krainie czarów.");
}

