﻿#include "Semaphore.h"

void Semaphore::Notify()
{
	std::unique_lock<decltype(_mutex)> lock(_mutex);
	++_count;
	_condition.notify_one();
}

void Semaphore::Wait()
{
	std::unique_lock<decltype(_mutex)> lock(_mutex);
	while (!_count)
		_condition.wait(lock);
	--_count;
}

bool Semaphore::TryWait()
{
	std::unique_lock<decltype(_mutex)> lock(_mutex);
	if (_count)
	{
		--_count;
		return true;
	}
	return false;
}
