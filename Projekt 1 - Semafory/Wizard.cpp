﻿#include "Wizard.h"
#include "ElderScrolls.h"

void Wizard::WizardThreadLoop()
{
	while (_work)
	{
		Interval().Wait();
		if (_work)
		{
			ElderScrolls::Write(L"Dobry czarodziej zdejmuje klątwy.");
			for (Factory* factory : _factories)
			{
				factory->CounterSemaphore.Wait();
				if (factory->CurseCounter > 0)
				{
					factory->CurseCounter -= 1;
					if (factory->CurseCounter == 0)
						factory->CurseSemaphore.Notify();
				}
				factory->CounterSemaphore.Notify();
			}
		}
	}
}

Wizard::Wizard(std::vector<Factory*>& factories, WizardConfiguration configuration) : _factories(factories), _interval(configuration.Dispell_MinInterval, configuration.Dispell_MaxInterval)
{
	ElderScrolls::Write(L"Budzi się dobry czarodziej.");
	_wizardThread = std::thread(&Wizard::WizardThreadLoop, this);
}

Wizard::~Wizard()
{
	_work = false;
	Interval().CancelWaiting();
	_wizardThread.join();
	ElderScrolls::Write(L"Dobry czarodziej idzie spać.");
}

RandomInterval& Wizard::Interval()
{
	return _interval;
}
