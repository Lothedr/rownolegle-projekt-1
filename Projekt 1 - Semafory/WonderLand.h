﻿#pragma once
#include "Warehouse.h"
#include <vector>
#include "Warlock.h"
#include "Wizard.h"
#include "Guild.h"

class WonderLand
{
	Warehouse* _warehouse;
	std::vector<Guild*> _guilds;
	std::vector<Factory*> _factories;
	std::vector<Warlock*> _warlocks;
	std::vector<Wizard*> _wizards;
public:
	void Start(Configuration configuration);
	void Stop();
};
