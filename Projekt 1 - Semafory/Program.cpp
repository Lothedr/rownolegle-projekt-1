#include <cstdlib>
#include "ElderScrolls.h"
#include "Guild.h"
#include <io.h>
#include <fcntl.h>
#include <atomic>
#include "WonderLand.h"


int main(int argc, char** argv)
{
	srand(time(nullptr));
	_setmode(_fileno(stdout), _O_U16TEXT);
	system("pause");
	tinyxml2::XMLDocument doc;
	doc.LoadFile("../config.xml");
	auto config = Configuration::FromXmlElement(doc.RootElement());
	WonderLand* wonderLand = new WonderLand;
	wonderLand->Start(config.first);
	getchar();
	wonderLand->Stop();
	delete wonderLand;
	system("pause");
	return 0;
}
