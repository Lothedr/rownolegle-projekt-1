﻿#pragma once
#include <condition_variable>
#include <random>

class RandomInterval
{
	std::condition_variable _waitingVariable;
	unsigned int _minTime = 5000;
	unsigned int _maxTime = 15000;
public:
	RandomInterval() = default;
	RandomInterval(unsigned int minTime, unsigned int maxTime);
	void Wait();
	void CancelWaiting();
	unsigned int& MinTime();
	unsigned int& MaxTime();
};
