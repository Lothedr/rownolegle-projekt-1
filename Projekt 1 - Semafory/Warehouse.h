﻿#pragma once
#include <array>
#include "Factory.h"
#include <list>
#include "ResourcePackage.h"
#include <map>
#include <queue>
#include "Configuration.h"

class Guild;

class Alchemist;

class Warehouse
{
	bool _work = true;
	std::vector<Factory*> _factories;
	std::array<int, 3> _resourcePool = {0, 0, 0};
	std::map<Guild*, Semaphore*> _semaphorMap;
	std::map<Guild*, int> _queueMap;
	std::thread _warehouseThread;
	Semaphore _semChange;
	Semaphore _semQueue;
	Semaphore _semResources;
	void WarehouseThreadLoop();
	bool EnoughResources(Guild* guild);
	ResourcePackage GetResources(ResourcePackage list);
public:
	Warehouse(std::vector<FactoryConfiguration>& configurations);
	~Warehouse();
	Semaphore* GuildQueue(Guild* guild);
	void AddAlchemist(Alchemist* alchemist);
	void AddResource(Resource res);
	int& LeadCount();
	int& SulphurCount();
	int& MercuryCount();
	Factory* LeadFactory();
	Factory* SulphurFactory();
	Factory* MercuryFactory();
	std::vector<Factory*>& Factories();
};
