﻿#include "Configuration.h"
#include <cstdlib>

std::pair<WarlockConfiguration, bool> WarlockConfiguration::FromXmlElement(tinyxml2::XMLElement* element)
{
	std::pair<WarlockConfiguration, bool> result(WarlockConfiguration(-1, -1), false);
	if (strcmp(element->Name(), "Warlock") == 0)
	{
		auto min = element->Attribute("MinInterval");
		auto max = element->Attribute("MaxInterval");
		if (min != nullptr && max != nullptr)
		{
			result.first.Curse_MinInterval = std::atoi(min);
			result.first.Curse_MaxInterval = std::atoi(max);
			result.second = true;
		}
	}
	return result;
}

WarlockConfiguration::WarlockConfiguration(int minInterval, int maxInterval)
{
	Curse_MinInterval = minInterval;
	Curse_MaxInterval = maxInterval;
}

std::pair<WizardConfiguration, bool> WizardConfiguration::FromXmlElement(tinyxml2::XMLElement* element)
{
	std::pair<WizardConfiguration, bool> result(WizardConfiguration(-1, -1), false);
	if (strcmp(element->Name(), "Wizard") == 0)
	{
		auto min = element->Attribute("MinInterval");
		auto max = element->Attribute("MaxInterval");
		if (min != nullptr && max != nullptr)
		{
			result.first.Dispell_MinInterval = std::atoi(min);
			result.first.Dispell_MaxInterval = std::atoi(max);
			result.second = true;
		}
	}
	return result;
}

WizardConfiguration::WizardConfiguration(int minInterval, int maxInterval)
{
	Dispell_MinInterval = minInterval;
	Dispell_MaxInterval = maxInterval;
}

std::pair<GuildConfiguration, bool> GuildConfiguration::FromXmlElement(tinyxml2::XMLElement* element)
{
	std::pair<GuildConfiguration, bool> result(GuildConfiguration(), false);
	if (strcmp(element->Name(), "Guild") == 0)
	{
		auto min = element->Attribute("AlchemistMinInterval");
		auto max = element->Attribute("AlchemistMaxInterval");
		auto name = element->Attribute("Name");
		auto sulphurNeed = element->Attribute("Sulphur");
		auto mercuryNeed = element->Attribute("Mercury");
		auto leadNeed = element->Attribute("Lead");
		if (min != nullptr && max != nullptr && name != nullptr)
		{
			result.first.AlchemistSpawn_MinInterval = std::atoi(min);
			result.first.AlchemistSpawn_MaxInterval = std::atoi(max);
			result.first.Name = std::wstring(strlen(name), 0);
			std::mbstowcs(&result.first.Name[0], name, result.first.Name.size());
			if (leadNeed != nullptr)
				result.first.LeadNeed = std::atoi(leadNeed);
			else
				result.first.LeadNeed = 0;
			if (mercuryNeed)
				result.first.MercuryNeed = std::atoi(mercuryNeed);
			else
				result.first.MercuryNeed = 0;
			if (sulphurNeed != nullptr)
				result.first.SulphurNeed = std::atoi(sulphurNeed);
			else
				result.first.SulphurNeed = 0;
			result.second = true;
		}
	}
	return result;
}

std::pair<FactoryConfiguration, bool> FactoryConfiguration::FromXmlElement(tinyxml2::XMLElement* element)
{
	if (!_mapInitialized)
	{
		_mapResource.insert(std::pair<std::string, Resource>("Lead", Lead));
		_mapResource.insert(std::pair<std::string, Resource>("Sulphur", Sulphur));
		_mapResource.insert(std::pair<std::string, Resource>("Mercury", Mercury));
		_mapInitialized = true;
	}
	std::pair<FactoryConfiguration, bool> result(FactoryConfiguration(-1, -1, Lead), false);
	if (strcmp(element->Name(), "Factory") == 0)
	{
		auto min = element->Attribute("MinInterval");
		auto max = element->Attribute("MaxInterval");
		auto resource = element->Attribute("Resource");
		if (min != nullptr && max != nullptr && resource != nullptr)
		{
			auto it = _mapResource.find(resource);
			if (it != _mapResource.end())
			{
				result.first.Production_MinInterval = std::atoi(min);
				result.first.Production_MaxInterval = std::atoi(max);
				result.first.ResourceType = it->second;
				result.second = true;
			}
		}
	}
	return result;
}

bool FactoryConfiguration::_mapInitialized = false;
std::map<std::string, Resource> FactoryConfiguration::_mapResource;

FactoryConfiguration::FactoryConfiguration(int minInterval, int maxInterval, Resource resource)
{
	Production_MinInterval = minInterval;
	Production_MaxInterval = maxInterval;
	ResourceType = resource;
}

std::pair<Configuration, bool> Configuration::FromXmlElement(tinyxml2::XMLElement* element)
{
	std::pair<Configuration, bool> result(Configuration(), false);
	if (strcmp(element->Name(), "Configuration") == 0)
	{
		result.second = true;
		auto node = element->FirstChildElement("Warlocks");
		{
			for (auto it = dynamic_cast<tinyxml2::XMLElement*>(node->FirstChild()); it != nullptr; it = dynamic_cast<tinyxml2::XMLElement*>(it->NextSibling()))
			{
				auto res = WarlockConfiguration::FromXmlElement(it);
				if (res.second)
					result.first.Warlocks.push_back(res.first);
			}
		}
		node = element->FirstChildElement("Wizards");
		{
			for (auto it = dynamic_cast<tinyxml2::XMLElement*>(node->FirstChild()); it != nullptr; it = dynamic_cast<tinyxml2::XMLElement*>(it->NextSibling()))
			{
				auto res = WizardConfiguration::FromXmlElement(it);
				if (res.second)
					result.first.Wizards.push_back(res.first);
			}
		}
		node = element->FirstChildElement("Guilds");
		{
			for (auto it = dynamic_cast<tinyxml2::XMLElement*>(node->FirstChild()); it != nullptr; it = dynamic_cast<tinyxml2::XMLElement*>(it->NextSibling()))
			{
				auto res = GuildConfiguration::FromXmlElement(it);
				if (res.second)
					result.first.Guilds.push_back(res.first);
			}
		}
		node = element->FirstChildElement("Factories");
		{
			for (auto it = dynamic_cast<tinyxml2::XMLElement*>(node->FirstChild()); it != nullptr; it = dynamic_cast<tinyxml2::XMLElement*>(it->NextSibling()))
			{
				auto res = FactoryConfiguration::FromXmlElement(it);
				if (res.second)
					result.first.Factories.push_back(res.first);
			}
		}
	}
	return result;
}
