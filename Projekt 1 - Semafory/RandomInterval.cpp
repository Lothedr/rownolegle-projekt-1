﻿#include "RandomInterval.h"
#include <chrono>
#include "ElderScrolls.h"

using namespace std::chrono_literals;

RandomInterval::RandomInterval(unsigned minTime, unsigned maxTime)
{
	_minTime = minTime;
	_maxTime = maxTime;
}

void RandomInterval::Wait()
{
	const int waitTime = _minTime + (ElderScrolls::GetRandomValue() % (_maxTime - _minTime));
	std::mutex mtx;
	std::unique_lock<std::mutex> lck(mtx);
	_waitingVariable.wait_for(lck, waitTime * 1ms);
}

void RandomInterval::CancelWaiting()
{
	_waitingVariable.notify_all();
}

unsigned& RandomInterval::MinTime()
{
	return _minTime;
}

unsigned& RandomInterval::MaxTime()
{
	return _maxTime;
}
