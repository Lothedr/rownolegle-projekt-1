﻿#pragma once
#include <string>
#include <vector>
#include "Lib/tinyxml2.h"
#include "Resource.h"
#include <map>

class WarlockConfiguration
{
public:
	static std::pair<WarlockConfiguration, bool> FromXmlElement(tinyxml2::XMLElement* element);
	WarlockConfiguration(int minInterval, int maxInterval);
	int Curse_MinInterval;
	int Curse_MaxInterval;
};

class WizardConfiguration
{
public:
	static std::pair<WizardConfiguration, bool> FromXmlElement(tinyxml2::XMLElement* element);
	WizardConfiguration(int minInterval, int maxInterval);
	int Dispell_MinInterval;
	int Dispell_MaxInterval;
};

class GuildConfiguration
{
public:
	static std::pair<GuildConfiguration, bool> FromXmlElement(tinyxml2::XMLElement* element);
	std::wstring Name;
	int SulphurNeed;
	int LeadNeed;
	int MercuryNeed;
	int AlchemistSpawn_MinInterval;
	int AlchemistSpawn_MaxInterval;
};

class FactoryConfiguration
{
	static std::map<std::string, Resource> _mapResource;
	static bool _mapInitialized;
public:
	static std::pair<FactoryConfiguration, bool> FromXmlElement(tinyxml2::XMLElement* element);
	FactoryConfiguration(int minInterval, int maxInterval, Resource resource);
	Resource ResourceType;
	int Production_MinInterval;
	int Production_MaxInterval;
};

class Configuration
{
public:
	static std::pair<Configuration, bool> FromXmlElement(tinyxml2::XMLElement* element);
	std::vector<WarlockConfiguration> Warlocks;
	std::vector<WizardConfiguration> Wizards;
	std::vector<FactoryConfiguration> Factories;
	std::vector<GuildConfiguration> Guilds;
};
