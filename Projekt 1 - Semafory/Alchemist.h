﻿#pragma once
#include "Semaphore.h"
#include "ResourcePackage.h"
#include "Warehouse.h"

class Guild;

class Alchemist
{
	Guild* _guild;
	ResourcePackage _shoppingList;
	ResourcePackage _shoppingCart;
	std::thread _alchemistThread;
	void AlchemistThreadLoop();
	void GoToShop();
	void ReturnToGuild();
public:
	Alchemist(Guild* guild);
	~Alchemist();
	void WaitForSleep();
	Guild* FromGuild() const;
	const ResourcePackage* ShoppingList() const;
	const ResourcePackage* ShoppingCart() const;
	void GiveResources(const ResourcePackage resource);
};
