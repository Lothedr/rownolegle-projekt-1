﻿#include "Warlock.h"
#include "ElderScrolls.h"
#include "MyUtility.h"

void Warlock::WarlockThreadLoop()
{
	while (_work)
	{
		Interval().Wait();
		if (_work)
		{
			Factory* chosen = _factories[ElderScrolls::GetRandomValue() % _factories.size()];
			ElderScrolls::Write(L"Zły czarownik nakłada klątwe na \""_w + chosen->GetName() + L"\"."_w);
			chosen->CounterSemaphore.Wait();
			if (chosen->CurseCounter == 0)
				chosen->CurseSemaphore.Wait();
			chosen->CurseCounter += 1;
			chosen->CounterSemaphore.Notify();
		}
	}
}

Warlock::Warlock(std::vector<Factory*>& factories, WarlockConfiguration configuration) : _factories(factories), _interval(configuration.Curse_MinInterval, configuration.Curse_MaxInterval)
{
	ElderScrolls::Write(L"Budzi się zły czarownik.");
	_warlockThread = std::thread(&Warlock::WarlockThreadLoop, this);
}

Warlock::~Warlock()
{
	_work = false;
	Interval().CancelWaiting();
	_warlockThread.join();
	ElderScrolls::Write(L"Zły czarownik idzie spać.");

}

RandomInterval& Warlock::Interval()
{
	return _interval;
}
