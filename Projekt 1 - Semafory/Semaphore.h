﻿#pragma once
#include <mutex>
#include <condition_variable>

class Semaphore
{
private:
	std::mutex _mutex;
	std::condition_variable _condition;
	unsigned long _count = 0;
public:
	void Notify();
	void Wait();
	bool TryWait();
};