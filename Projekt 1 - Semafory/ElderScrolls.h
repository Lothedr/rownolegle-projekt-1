﻿#pragma once
#include <string>
#include <thread>
#include <sstream>
#include <atomic>
#include "Semaphore.h"
#include <random>

class ElderScrolls
{
	static std::mutex _mtxGenerator;
	static std::linear_congruential_engine<std::uint_fast32_t, 16807, 0, 2147483647> _generator;
public:
	static void Write(std::wostream&& stream);
	static void Write(std::wostream& stream);
	static void Write(std::wstring&& phrase);
	static void Write(std::wstring& phrase);
	static void Write(const wchar_t* phrase);
	static int GetRandomValue();
};
