﻿#pragma once

#include <string>
#include <iostream>

inline std::wstring operator""_w(const wchar_t* str, std::size_t size)
{
	return std::wstring(str, size);
}

inline std::wstring operator""_w(unsigned long long int i)
{
	return std::to_wstring(i);
}
