﻿#include "Guild.h"
#include "ElderScrolls.h"
#include <random>
#include "MyUtility.h"
#include "Alchemist.h"

void Guild::WakeUpAlchemist()
{
	_semAlchemists.Wait();
	ElderScrolls::Write(L"\""_w + _name + L"\" wysyła alchemika."_w);
	Alchemist* alchemist = new Alchemist(this);
	_alchemists.push_back(alchemist);
	_semAlchemists.Notify();
}

void Guild::GuildThreadLoop()
{
	while (_work)
	{
		Interval().Wait();
		if (_work)
			WakeUpAlchemist();
	}
}

void Guild::GuildWaitingThreadLoop()
{
	while (_work || !_alchemists.empty())
	{
		_semAlchemistArrive.Wait();
		_semWaitingAlchemists.Wait();
		while (!_toWait.empty())
		{
			Alchemist* alchemist = _toWait.front();
			_toWait.pop();
			_alchemists.erase(std::find(_alchemists.begin(), _alchemists.end(), alchemist));
			alchemist->WaitForSleep();
			delete alchemist;
		}
		_semWaitingAlchemists.Notify();
	}
}

Guild::Guild(Warehouse* warehouse, GuildConfiguration configuration) : _name(configuration.Name), _shoppingList{configuration.LeadNeed, configuration.SulphurNeed, configuration.MercuryNeed}, _interval(configuration.AlchemistSpawn_MinInterval, configuration.AlchemistSpawn_MaxInterval)
{
	_warehouse = warehouse;
}

Guild::~Guild()
{
	if (_work)
		Stop();
}

void Guild::Start()
{
	_semAlchemists.Notify();
	_semWaitingAlchemists.Notify();
	ElderScrolls::Write(L"Otwiera się \""_w + _name + L"\"."_w);
	_guildThread = std::thread(&Guild::GuildThreadLoop, this);
	_guildWaitingThread = std::thread(&Guild::GuildWaitingThreadLoop, this);
}

void Guild::Stop()
{
	_work = false;
	Interval().CancelWaiting();
	_guildThread.join();
	if (_alchemists.empty())
	{
		_semAlchemistArrive.Notify();
		_semWaitingAlchemists.Notify();
	}
	ElderScrolls::Write(L"Zamyka się \""_w + _name + L"\"."_w);
}

void Guild::WaitForAlchemists()
{

	_guildWaitingThread.join();
	ElderScrolls::Write(L"\""_w + _name + L"\" skonczyła prace ze stanem "_w + std::to_wstring(_gold) + L" złota."_w);
}

RandomInterval& Guild::Interval()
{
	return _interval;
}

Warehouse* Guild::Shop() const
{
	return _warehouse;
}

std::wstring& Guild::Name()
{
	return _name;
}

const ResourcePackage* Guild::ShoppingList() const
{
	return &_shoppingList;
}

void Guild::ReturnBack(Alchemist* alchemist)
{
	_semWaitingAlchemists.Wait();
	_toWait.push(alchemist);
	_semAlchemistArrive.Notify();
	_semWaitingAlchemists.Notify();
	if (*(alchemist->ShoppingCart()) == _shoppingList)
	{
		ElderScrolls::Write(L"Do \""_w + _name + L"\" wrócił alchemik z odczynnikami na złoto. Gildia posiada "_w + std::to_wstring(++_gold) + L" złota"_w);
	}
	else
	{
		ElderScrolls::Write(L"Do \""_w + _name + L"\" wrócił alchemik, niestety bez odczynników."_w);
	}
}
