﻿#pragma once

class ResourcePackage
{
public:
	bool operator==(const ResourcePackage& package) const;
	int LeadCount = 0;
	int SulphurCount = 0;
	int MercuryCount = 0;
};
