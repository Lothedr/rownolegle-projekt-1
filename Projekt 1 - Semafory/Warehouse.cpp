﻿#include "Warehouse.h"
#include "ElderScrolls.h"
#include "Guild.h"
#include "MyUtility.h"
#include "Alchemist.h"

void Warehouse::WarehouseThreadLoop()
{
	while (_work)
	{
		_semChange.Wait();
		if (!_work)
			break;
		_semQueue.Wait();
		for (auto& it : _queueMap)
		{
			if (it.second > 0 && EnoughResources(it.first))
			{
				it.second -= 1;
				_semaphorMap[it.first]->Notify();
			}
		}
		_semQueue.Notify();
	}
}

bool Warehouse::EnoughResources(Guild* guild)
{
	const ResourcePackage list = *guild->ShoppingList();
	return list.LeadCount <= LeadCount() && list.SulphurCount <= SulphurCount() && list.MercuryCount <= MercuryCount();
}

ResourcePackage Warehouse::GetResources(const ResourcePackage list)
{
	_semResources.Wait();
	if (list.LeadCount <= LeadCount() && list.SulphurCount <= SulphurCount() && list.MercuryCount <= MercuryCount())
	{
		LeadCount() -= list.LeadCount;
		for (int i = 0; i < list.LeadCount; ++i)
			LeadFactory()->MakeOrder();
		SulphurCount() -= list.SulphurCount;
		for (int i = 0; i < list.SulphurCount; ++i)
			SulphurFactory()->MakeOrder();
		MercuryCount() -= list.MercuryCount;
		for (int i = 0; i < list.MercuryCount; ++i)
			MercuryFactory()->MakeOrder();
		_semResources.Notify();
		return list;
	}
	_semResources.Notify();
	return {};
}

Warehouse::Warehouse(std::vector<FactoryConfiguration>& configurations) : _factories(3)
{
	ElderScrolls::Write(L"Otwiera się sklep ze składnikami alchemicznymi");
	_warehouseThread = std::thread(&Warehouse::WarehouseThreadLoop, this);
	_semQueue.Notify();
	for (auto && configuration : configurations)
	{
		switch (configuration.ResourceType)
		{
		case Lead:
			_factories[0] = new Factory(this, configuration);
			break;
		case Sulphur:
			_factories[1] = new Factory(this, configuration);
			break;
		case Mercury:
			_factories[2] = new Factory(this, configuration);
			break;
		default: ;
		}
	}
	for (Factory* factory : _factories)
	{
		factory->Start();
		factory->MakeOrder();
		factory->MakeOrder();
	}
	_semResources.Notify();
}

Warehouse::~Warehouse()
{
	_work = false;
	_semChange.Notify();
	_warehouseThread.join();
	for (auto& it : _queueMap)
	{
		while (it.second > 0)
		{
			it.second -= 1;
			_semaphorMap[it.first]->Notify();
		}
	}
	for (Factory* factory : _factories)
	{
		factory->Stop();
	}
	ElderScrolls::Write(L"Skelp alchemiczny kończy prace.");
}

Semaphore* Warehouse::GuildQueue(Guild* guild)
{
	return _semaphorMap[guild];
}

void Warehouse::AddAlchemist(Alchemist* alchemist)
{
	ElderScrolls::Write(L"Do sklepu przychodzi alchemik gildi \""_w + alchemist->FromGuild()->Name() + L"\"."_w);
	_semQueue.Wait();
	auto guildIt = _semaphorMap.find(alchemist->FromGuild());
	auto queIt = _queueMap.find(alchemist->FromGuild());
	if (guildIt == _semaphorMap.end())
	{
		guildIt = _semaphorMap.insert(std::make_pair(alchemist->FromGuild(), new Semaphore)).first;
		queIt = _queueMap.insert(std::make_pair(alchemist->FromGuild(), 0)).first;
	}
	queIt->second += 1;
	_semQueue.Notify();
	_semChange.Notify();
	guildIt->second->Wait();
	alchemist->GiveResources(GetResources(*(alchemist->ShoppingList())));
}

void Warehouse::AddResource(Resource res)
{
	_semResources.Wait();
	switch (res) {
		case Lead:
			ElderScrolls::Write(L"Do sklepu dostarczono 1 ołowiu. Aktualny stan to "_w + std::to_wstring(LeadCount() += 1) + L"."_w);
			break;												 
		case Sulphur:											 
			ElderScrolls::Write(L"Do sklepu dostarczono 1 siarki. Aktualny stan to "_w + std::to_wstring(SulphurCount() += 1) + L"."_w);
			break;
		case Mercury:											 
			ElderScrolls::Write(L"Do sklepu dostarczono 1 rtęć. Aktualny stan to "_w + std::to_wstring(MercuryCount() += 1) + L"."_w);
			break;
		default: ;
	}
	_semResources.Notify();
	_semChange.Notify();
 }

int& Warehouse::LeadCount()
{
	return _resourcePool[0];
}

int& Warehouse::SulphurCount()
{
	return _resourcePool[1];
}

int& Warehouse::MercuryCount()
{
	return _resourcePool[2];
}

Factory* Warehouse::LeadFactory()
{
	return _factories[0];
}

Factory* Warehouse::SulphurFactory()
{
	return _factories[1];
}

Factory* Warehouse::MercuryFactory()
{
	return _factories[2];
}

std::vector<Factory*>& Warehouse::Factories()
{
	return _factories;
}

