﻿#include "Alchemist.h"
#include "Guild.h"
#include "ElderScrolls.h"
#include "MyUtility.h"

void Alchemist::AlchemistThreadLoop()
{
	GoToShop();
	ReturnToGuild();
}

void Alchemist::GoToShop()
{
	_guild->Shop()->AddAlchemist(this);
}

void Alchemist::ReturnToGuild()
{
	_guild->ReturnBack(this);
}

Alchemist::Alchemist(Guild* guild) : _guild(guild)
{
	//ElderScrolls::Write(L"Budzi się alchemik z \""_w + _guild->Name() + L"\"."_w);
	_alchemistThread = std::thread(&Alchemist::AlchemistThreadLoop, this);
}

Alchemist::~Alchemist()
{
	WaitForSleep();
	//ElderScrolls::Write(L"Alchemik z \""_w + _guild->Name() + L"\" idzie spać."_w);
}

void Alchemist::WaitForSleep()
{
	if (_alchemistThread.joinable())
		_alchemistThread.join();
}

Guild* Alchemist::FromGuild() const
{
	return _guild;
}

const ResourcePackage* Alchemist::ShoppingList() const
{
	return _guild->ShoppingList();
}

const ResourcePackage* Alchemist::ShoppingCart() const
{
	return &_shoppingCart;
}

void Alchemist::GiveResources(const ResourcePackage resource)
{
	_shoppingCart = resource;
}
