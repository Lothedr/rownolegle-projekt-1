﻿#pragma once
#include "Semaphore.h"
#include "RandomInterval.h"
#include "Resource.h"
#include "Configuration.h"

class Warehouse;

class Factory
{
	bool _work = true;
	Semaphore _semResource;
	Warehouse* _warehouse;
	Resource _resourceType;
	RandomInterval _interval;
	std::thread _factoryThread;
	void FactoryThreadLoop();
public:
	Semaphore CounterSemaphore;
	int CurseCounter = 0;
	Semaphore CurseSemaphore;
	std::wstring GetName(bool capital = false) const;
	Factory(Warehouse* warehouse, FactoryConfiguration configuration);
	~Factory();
	void Start();
	void Stop();
	RandomInterval& Interval();
	Resource ResourceType() const;
	void MakeOrder();
};
