﻿#include "ElderScrolls.h"
#include <iostream>
#include <sstream>
#include <ostream>

void ElderScrolls::Write(std::wostream&& stream)
{
	stream << L"\n";
	std::wcout << stream.rdbuf();
}

void ElderScrolls::Write(std::wostream& stream)
{
	stream << L"\n";
	std::wcout << stream.rdbuf();
}

void ElderScrolls::Write(std::wstring&& phrase)
{
	phrase.append(L"\n");
	std::wcout << phrase;
}

void ElderScrolls::Write(std::wstring& phrase)
{
	phrase.append(L"\n");
	std::wcout << phrase;
}

void ElderScrolls::Write(const wchar_t* phrase)
{
	std::wstring str(phrase);
	Write(std::move(str));
}

int ElderScrolls::GetRandomValue()
{
	_mtxGenerator.lock();
	int result = _generator();
	_mtxGenerator.unlock();
	return result;
}

std::mutex ElderScrolls::_mtxGenerator;
std::linear_congruential_engine<std::uint_fast32_t, 16807, 0, 2147483647> ElderScrolls::_generator;