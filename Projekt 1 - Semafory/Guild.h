﻿#pragma once
#include "ResourcePackage.h"
#include "RandomInterval.h"
#include "Warehouse.h"
#include <vector>
#include <queue>

class Guild
{
	bool _work = true;
	Semaphore _semAlchemists;
	Semaphore _semAlchemistArrive;
	Semaphore _semWaitingAlchemists;
	ResourcePackage _shoppingList;
	RandomInterval _interval;
	Warehouse* _warehouse;
	int _gold = 0;
	void WakeUpAlchemist();
	std::wstring _name;
	std::thread _guildThread;
	std::thread _guildWaitingThread;
	void GuildThreadLoop();
	void GuildWaitingThreadLoop();
	std::vector<Alchemist*> _alchemists;
	std::queue<Alchemist*> _toWait;
public:
	Guild(Warehouse* warehouse, GuildConfiguration configuration);
	~Guild();
	void Start();
	void Stop();
	void WaitForAlchemists();
	RandomInterval& Interval();
	Warehouse* Shop() const;
	std::wstring& Name();
	const ResourcePackage* ShoppingList() const;
	void ReturnBack(Alchemist* alchemist);
};
