﻿#pragma once
#include <array>
#include "Factory.h"
#include "Configuration.h"

class Warlock
{
	bool _work = true;
	std::vector<Factory*> _factories;
	RandomInterval _interval;
	std::thread _warlockThread;
	void WarlockThreadLoop();
public:
	Warlock(std::vector<Factory*>& factories, WarlockConfiguration configuration);
	~Warlock();
	RandomInterval& Interval();
};
