﻿#pragma once
#include <array>
#include "Factory.h"
#include "Configuration.h"

class Wizard
{
	bool _work = true;
	std::vector<Factory*> _factories;
	RandomInterval _interval;
	std::thread _wizardThread;
	void WizardThreadLoop();
public:
	Wizard(std::vector<Factory*>& factories, WizardConfiguration configuration);
	~Wizard();
	RandomInterval& Interval();
};
