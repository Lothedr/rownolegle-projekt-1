﻿#include "Factory.h"
#include "Warehouse.h"
#include "ElderScrolls.h"
#include "MyUtility.h"

void Factory::FactoryThreadLoop()
{
	while (_work)
	{
		_semResource.Wait();
		if (_work)
		{
			CurseSemaphore.Wait();
			CurseSemaphore.Notify();
		}
		if (_work)
		{
			ElderScrolls::Write(GetName(true) + L" rozpoczyna produkcje zasobu."_w);
			Interval().Wait();
		}
		if (_work)
			_warehouse->AddResource(ResourceType());
	}
}

std::wstring Factory::GetName(bool capital) const
{
	std::wstring msg = std::wstring(capital ? L"Fabryka " : L"fabryka ");
	switch (_resourceType)
	{
	case Lead:
		msg.append(L"ołowiu");
		break;
	case Sulphur:
		msg.append(L"siarki");
		break;
	case Mercury:
		msg.append(L"rtęci");
		break;
	default:;
	}
	return msg;
}

Factory::Factory(Warehouse* warehouse, FactoryConfiguration configuration) : _interval(configuration.Production_MinInterval, configuration.Production_MaxInterval), _resourceType(configuration.ResourceType)
{
	_warehouse = warehouse;
	CurseSemaphore.Notify();
	CounterSemaphore.Notify();
}

Factory::~Factory()
{
	if (_work)
		Stop();
}

void Factory::Start()
{
	ElderScrolls::Write(L"Otwiera się "_w + GetName() + L"."_w);
	_factoryThread = std::thread(&Factory::FactoryThreadLoop, this);
}

void Factory::Stop()
{
	_work = false;
	_semResource.Notify();
	CurseSemaphore.Notify();
	Interval().CancelWaiting();
	_factoryThread.join();
	ElderScrolls::Write(L"Zamyka się "_w + GetName() + L"."_w);
}

RandomInterval& Factory::Interval()
{
	return _interval;
}

Resource Factory::ResourceType() const
{
	return _resourceType;
}

void Factory::MakeOrder()
{
	_semResource.Notify();
}
