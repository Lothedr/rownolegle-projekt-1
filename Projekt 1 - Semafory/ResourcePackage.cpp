﻿#include "ResourcePackage.h"

bool ResourcePackage::operator==(const ResourcePackage& package) const
{
	return LeadCount == package.LeadCount && SulphurCount == package.SulphurCount && MercuryCount == package.MercuryCount;
}
